// Copyright Epic Games, Inc. All Rights Reserved.

#include "tds.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, tds, "tds" );

DEFINE_LOG_CATEGORY(Logtds)
 